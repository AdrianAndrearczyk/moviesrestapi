package domain;

import java.util.ArrayList;

import domain.services.ActorService;

public class Actor {

		private int id;
		private String name;
		private String surname;
		
		public int getId() {
			return id;
		}
		public void setId(int id) {
			this.id = id;
		}
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public String getSurname() {
			return surname;
		}
		public void setSurname(String surname) {
			this.surname = surname;
		}
		
		//external
		public ActorService getMovie() {
			return null;
		}

		public void setMovie(ArrayList<Actor> arrayList) {

		}
}
