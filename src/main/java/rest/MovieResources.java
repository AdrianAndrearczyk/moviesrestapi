package rest;

import domain.Actor;
import domain.Comment;
import domain.Movie;
import domain.Rating;
import domain.services.MovieService;





import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import java.util.ArrayList;
import java.util.List;

@Path("/movie")
public class MovieResources {

    private MovieService db = new MovieService();

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Movie> getAll(){
        return db.getAll();
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response Add(Movie movie){
        db.add(movie);
        return Response.ok(movie.getId()).build();

    }
    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response get(@PathParam("id") int id){
    	Movie result = db.get(id);
        if(result==null){
            return Response.status(404).build();
        }
        return Response.ok(result).build();
    }

    @PUT
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response update(@PathParam("id") int id, Movie m){
    	Movie result = db.get(id);
        if(result==null){
            return Response.status(404).build();
        }
        m.setId(id);
        db.update(m);
        return Response.ok(result).build();
    }

    @DELETE
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response delete(@PathParam("id") int id){
    	Movie result = db.get(id);
        if(result==null){
            return Response.status(404).build();
        }
        db.update(result);
        return Response.ok(result).build();
    }

    @GET
    @Path("/{movieId}/actors")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Actor> getActors(@PathParam("movieId") int movieId){
    	Movie result = db.get(movieId);
        if(result==null){
            return null;
        }
        if (result.getActors()==null)
            result.setActors(new ArrayList<Actor>());
        return result.getActors();
    }

    @POST
    @Path("/{movieId}/actors")
    @Produces(MediaType.APPLICATION_JSON)
    public Response addActor(@PathParam("movieId") int movieId, Actor actor){
    	Movie result = db.get(movieId);
        if(result==null){
            return Response.status(404).build();
        }
        if (result.getActors()==null)
            result.setActors(new ArrayList<Actor>());
        result.getActors().add(actor);
        return Response.ok().build();
    }
    
    @PUT
	@Path("/{movieId}/actors")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response updateActor(@PathParam("id") int id, Movie m){
		Movie result = db.get(id);
		if(result==null)
			return Response.status(404).build();
		m.setId(id);
		db.update(m);
		return Response.ok().build();
	}
    
    @DELETE
    @Path("/{movieId}/actors/{actorId}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteActor(@PathParam("actorId") int movieId, int actorId){
    	Movie result = db.get(actorId);
        if(result==null){
            return Response.status(404).build();
        }
        db.update(result);
        return Response.ok(result).build();
    }
    
    @GET
    @Path("/{movieId}/comments")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Comment> getComments(@PathParam("movieId") int movieId){
    	Movie result = db.get(movieId);
        if(result==null){
            return null;
        }
        if (result.getComments()==null)
            result.setComments(new ArrayList<Comment>());
        return result.getComments();
    }

    @POST
    @Path("/{movieId}/comments")
    @Produces(MediaType.APPLICATION_JSON)
    public Response addComment(@PathParam("movieId") int movieId, Comment comment){
    	Movie result = db.get(movieId);
        if(result==null){
            return Response.status(404).build();
        }
        if (result.getComments()==null)
            result.setComments(new ArrayList<Comment>());
        result.getComments().add(comment);
        return Response.ok().build();
    }

    @PUT
	@Path("/{movieId}/comments")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response updateComments(@PathParam("id") int id, Movie m){
		Movie result = db.get(id);
		if(result==null)
			return Response.status(404).build();
		m.setId(id);
		db.update(m);
		return Response.ok().build();
	}
    
    @DELETE
    @Path("/{movieId}/comments/{commentId}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteComment(@PathParam("commentId") int movieId, int commentId){
    	Movie result = db.get(commentId);
        if(result==null){
            return Response.status(404).build();
        }
        db.update(result);
        return Response.ok(result).build();
    }
    
    //ratings
    @GET
    @Path("/{movieId}/ratings")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Rating> getRatings(@PathParam("movieId") int movieId){
    	Movie result = db.get(movieId);
        if(result==null){
            return null;
        }
        if (result.getRatings()==null)
            result.setRatings(new ArrayList<Rating>());

        
        return result.getRatings();
    }



    @POST
    @Path("/{movieId}/ratings")
    @Produces(MediaType.APPLICATION_JSON)
    public Response addRating(@PathParam("movieId") int movieId, Rating rating){
    	Movie result = db.get(movieId);
        if(result==null){
            return Response.status(404).build();
        }
        if (result.getRatings()==null)
            result.setRatings(new ArrayList<Rating>());
        result.getRatings().add(rating);
        return Response.ok().build();
    }

    @DELETE
    @Path("/{movieId}/ratings/{ratingId}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteRating(@PathParam("ratingId") int movieId, int ratingId){
    	Movie result = db.get(ratingId);
        if(result==null){
            return Response.status(404).build();
        }
        db.update(result);
        return Response.ok(result).build();
    }
}